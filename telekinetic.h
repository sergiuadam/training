#ifndef TELEKINETIC_H
#define TELEKINETIC_H
#include <mutant.h>

class Telekinetic : virtual public Mutant{
public:
    Telekinetic();
    Telekinetic(int x);
    ~Telekinetic() override;
};

#endif // TELEKINETIC_H
