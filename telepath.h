#ifndef TELEPATH_H
#define TELEPATH_H
#include "mutant.h"
#include "observer.h"

#include <iostream>
#include <string>


class Telepath : virtual public Observer, virtual public Mutant {
public:
    Telepath();
    ~Telepath() override;
    int getNumberOfTelepaths() const;
    void notify() override;
    void enterRoom(Cerebro*)override;
    void quitRoom()override;
};

#endif // TELEPATH_H
