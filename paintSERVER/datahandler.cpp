#include "datahandler.h"
#include <iostream>
DataHandler::DataHandler() = default;

int DataHandler::enconde(QByteArray& destination, const Shape::ShapeTypes &shapeType, const QString &shapeColor, const Coordinates &coords, const QString &text) {
    QBuffer buffer(&destination);
    buffer.open(QIODevice::WriteOnly);
    QDataStream out(&buffer);

    if (shapeType == Shape::ShapeTypes::Text) {
        out<<static_cast<int>(shapeType) <<
             shapeColor <<
             coords.topLeftCoordinateX <<
             coords.topLeftCoordinateY <<
             coords.bottomRightCoordinateX <<
             coords.bottomRightCoordinateY <<
             text;
    }
    else{
        out<<static_cast<int>(shapeType) <<
             shapeColor <<
             coords.topLeftCoordinateX <<
             coords.topLeftCoordinateY <<
             coords.bottomRightCoordinateX <<
             coords.bottomRightCoordinateY;
    }
    int messageLength = destination.size();
    out<<messageLength;
    return messageLength;
}

int DataHandler::decode(QByteArray &source, Shape::ShapeTypes &shapeType, QString &shapeColor, Coordinates &coords, QString &text) {
    QBuffer buffer(&source);
    buffer.open(QIODevice::ReadOnly);
    QDataStream in(&buffer);
    int type;
    in >> type
            >> shapeColor
            >> coords.topLeftCoordinateX
            >> coords.topLeftCoordinateY
            >> coords.bottomRightCoordinateX
            >> coords.bottomRightCoordinateY;
    shapeType = static_cast<Shape::ShapeTypes>(type);

    if(shapeType == Shape::ShapeTypes::Text){
        in >> text;
    }
    else{
        text = "";
    }
    int messageLength;
    in>>messageLength;
    return messageLength;
}
