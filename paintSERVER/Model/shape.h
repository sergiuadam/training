#pragma once
#include <QObject>
#include <string>
#include <QString>
#include <QStringList>
using namespace std;

struct Coordinates{
    int topLeftCoordinateX{}, bottomRightCoordinateX{};
    int topLeftCoordinateY{}, bottomRightCoordinateY{};
};

class Shape {
public:
    enum ShapeTypes {Rectangle, Square, Circle, Line, Point, Text, Invalid};

private:
    ShapeTypes m_type;
    QString m_color;
    Coordinates m_coordinates;
    QString m_text;

public:
    Shape(QString,Coordinates);
    ~Shape();

    void setType(const ShapeTypes type);
    void setColor(QString color);
    void setText(QString text);
    void setCoordinateX(int coordX);
    void setCoordinateY(int coordY);

    ShapeTypes Type() const;
    QString Color() const;
    QString Txt() const;
    int StartCoordinateX() const;
    int StartCoordinateY() const;
    int EndCoordinateX() const;
    int EndCoordinateY() const;
};

