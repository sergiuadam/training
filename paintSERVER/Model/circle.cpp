#include "circle.h"

#include <utility>

Circle::Circle(QString color, Coordinates coordinates):Shape(std::move(color), coordinates) {
    setType(ShapeTypes::Circle);
}
