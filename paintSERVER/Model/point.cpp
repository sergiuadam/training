#include "point.h"

#include <utility>

Point::Point(QString color, Coordinates coordinates):Shape(std::move(color), coordinates) {
    setType(ShapeTypes::Point);
}
