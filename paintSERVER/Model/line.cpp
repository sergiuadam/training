#include "line.h"

#include <utility>

Line::Line(QString color, Coordinates coordinates) : Shape(std::move(color), coordinates) {
    setType(ShapeTypes::Line);
}


