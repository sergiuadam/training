#include "square.h"

#include <utility>

Square::Square(QString color, Coordinates coordinates):Shape(std::move(color), coordinates) {
    setType(ShapeTypes::Square);
}
