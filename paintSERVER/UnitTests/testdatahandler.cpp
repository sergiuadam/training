#include "testdatahandler.h"
#include <QtTest>
#include <iostream>
void TestDataHandler::testEncodeWithoutText() {
    DataHandler dataHandler;

    //ENCODE AND SIMULATE SEND & RECEIVE
    QByteArray bytes;
    QString color("black");
    Coordinates coords;
    QString text("");
    coords.topLeftCoordinateX = 20;
    coords.topLeftCoordinateY = 80;
    coords.bottomRightCoordinateX = 10;
    coords.bottomRightCoordinateY = 90;

    //TESTED FUNCTION CALL
    int size = dataHandler.enconde(bytes,Shape::ShapeTypes::Circle,color,coords,text);

    QCOMPARE(size,bytes.size()-static_cast<int>(sizeof(int)));

    //RECEIVE LOCATION
    QBuffer expectedBuffer(&bytes);
    expectedBuffer.open(QBuffer::ReadOnly);
    QDataStream expectedStream(&expectedBuffer);

    int expectedType;
    expectedStream>>expectedType;
    QCOMPARE(static_cast<Shape::ShapeTypes>(expectedType) , Shape::ShapeTypes::Circle);

    QString expectedColor;
    expectedStream >> expectedColor;
    QCOMPARE(expectedColor , color);

    int expectedX1, expectedY1, expectedX2, expectedY2;

    expectedStream >> expectedX1;
    QCOMPARE(expectedX1 , coords.topLeftCoordinateX);

    expectedStream >> expectedY1;
    QCOMPARE(expectedY1 , coords.topLeftCoordinateY);

    expectedStream >> expectedX2;
    QCOMPARE(expectedX2 , coords.bottomRightCoordinateX);

    expectedStream >> expectedY2;
    QCOMPARE(expectedY2 , coords.bottomRightCoordinateY);

}

void TestDataHandler::testDecodeWithoutText() {
    DataHandler dataHandler;

    QByteArray bytes;
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::WriteOnly);
    QDataStream stream(&buffer);

    stream << static_cast<int>(Shape::ShapeTypes::Rectangle);
    stream << QString("red");
    stream << 1 << 2 << 3 << 4;
    stream << bytes.size();

    Shape::ShapeTypes expectedType;
    QString expectedColor;
    Coordinates expectedCoords;
    QString expectedText;

    //TESTED FUNCTION CALL
    int size = dataHandler.decode(bytes,expectedType,expectedColor,expectedCoords,expectedText);
    QCOMPARE(size,bytes.size()-static_cast<int>(sizeof(int)));
    QCOMPARE(expectedType,Shape::ShapeTypes::Rectangle);
    QCOMPARE(expectedColor, QString("red"));
    QCOMPARE(expectedCoords.topLeftCoordinateX, 1);
    QCOMPARE(expectedCoords.topLeftCoordinateY, 2);
    QCOMPARE(expectedCoords.bottomRightCoordinateX, 3);
    QCOMPARE(expectedCoords.bottomRightCoordinateY, 4);
    QCOMPARE(expectedText, QString(""));
}


void TestDataHandler::testEncodeWithText() {
    DataHandler dataHandler;

    //ENCODE AND SIMULATE SEND & RECEIVE
    QByteArray bytes;
    QString color("black");
    Coordinates coords;
    QString text("TESTtext");
    coords.topLeftCoordinateX = 20;
    coords.topLeftCoordinateY = 80;
    coords.bottomRightCoordinateX = 10;
    coords.bottomRightCoordinateY = 90;

    //TESTED FUNCTION CALL
    int size = dataHandler.enconde(bytes,Shape::ShapeTypes::Text,color,coords,text);

    QCOMPARE(size,bytes.size()-static_cast<int>(sizeof(int)));

    //RECEIVE LOCATION
    QBuffer expectedBuffer(&bytes);
    expectedBuffer.open(QBuffer::ReadOnly);
    QDataStream expectedStream(&expectedBuffer);

    int expectedType;
    expectedStream>>expectedType;
    QCOMPARE(static_cast<Shape::ShapeTypes>(expectedType) , Shape::ShapeTypes::Text);

    QString expectedColor;
    expectedStream >> expectedColor;
    QCOMPARE(expectedColor , color);

    int expectedX1, expectedY1, expectedX2, expectedY2;

    expectedStream >> expectedX1;
    QCOMPARE(expectedX1 , coords.topLeftCoordinateX);

    expectedStream >> expectedY1;
    QCOMPARE(expectedY1 , coords.topLeftCoordinateY);

    expectedStream >> expectedX2;
    QCOMPARE(expectedX2 , coords.bottomRightCoordinateX);

    expectedStream >> expectedY2;
    QCOMPARE(expectedY2 , coords.bottomRightCoordinateY);

    QString expectedText;
    expectedStream >> expectedText;
    QCOMPARE(expectedText , text);
}

void TestDataHandler::testDecodeWithText() {
    DataHandler dataHandler;

    QByteArray bytes;
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::WriteOnly);
    QDataStream stream(&buffer);

    stream << static_cast<int>(Shape::ShapeTypes::Text);
    stream << QString("red");
    stream << 1 << 2 << 3 << 4;
    stream << QString("textEXPECTED");
    stream << bytes.size();

    Shape::ShapeTypes expectedType;
    QString expectedColor;
    Coordinates expectedCoords;
    QString expectedText;

    //TESTED FUNCTION CALL
    int size = dataHandler.decode(bytes,expectedType,expectedColor,expectedCoords,expectedText);
    QCOMPARE(size,bytes.size()-static_cast<int>(sizeof(int)));
    QCOMPARE(expectedType,Shape::ShapeTypes::Text);
    QCOMPARE(expectedColor, QString("red"));
    QCOMPARE(expectedCoords.topLeftCoordinateX, 1);
    QCOMPARE(expectedCoords.topLeftCoordinateY, 2);
    QCOMPARE(expectedCoords.bottomRightCoordinateX, 3);
    QCOMPARE(expectedCoords.bottomRightCoordinateY, 4);
    QCOMPARE(expectedText, QString("textEXPECTED"));
}


QTEST_MAIN(TestDataHandler);
