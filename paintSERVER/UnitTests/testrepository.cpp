#include "testrepository.h"
#include "../Controller/repository.h"



void TestRepository::testAddShapeNonText() {
    Repository repo;
    Shape::ShapeTypes type = Shape::ShapeTypes::Rectangle;
    QString color("blue");
    Coordinates coords;
    coords.topLeftCoordinateX = 1;
    coords.topLeftCoordinateY = 2;
    coords.bottomRightCoordinateX = 3;
    coords.bottomRightCoordinateY = 4;

    QCOMPARE(repo.m_shapes.size(),0);

    repo.addShape(type,color,coords);

    QCOMPARE(repo.m_shapes.size(), 1);
    QCOMPARE(repo.m_shapes[0]->Type(), type);
    QCOMPARE(repo.m_shapes[0]->Color(), color);
    QCOMPARE(repo.m_shapes[0]->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(repo.m_shapes[0]->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(repo.m_shapes[0]->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(repo.m_shapes[0]->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestRepository::testAddShapeText() {
    Repository repo;
    QString color("blue");
    Coordinates coords;
    QString text("text");
    coords.topLeftCoordinateX = 1;
    coords.topLeftCoordinateY = 2;
    coords.bottomRightCoordinateX = 3;
    coords.bottomRightCoordinateY = 4;

    QCOMPARE(repo.m_shapes.size(),0);

    repo.addShape(color,coords,text);

    QCOMPARE(repo.m_shapes.size(), 1);
    QCOMPARE(repo.m_shapes[0]->Txt(), text);
    QCOMPARE(repo.m_shapes[0]->Color(), color);
    QCOMPARE(repo.m_shapes[0]->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(repo.m_shapes[0]->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(repo.m_shapes[0]->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(repo.m_shapes[0]->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestRepository::testGetLastShapeAdded() {
    Repository repo;
    Shape::ShapeTypes type = Shape::ShapeTypes::Rectangle;
    QString color("blue");
    Coordinates coords;
    coords.topLeftCoordinateX = 1;
    coords.topLeftCoordinateY = 2;
    coords.bottomRightCoordinateX = 3;
    coords.bottomRightCoordinateY = 4;

    QCOMPARE(repo.m_shapes.size(),0);

    repo.addShape(type,color,coords);

    const Shape* shape = repo.getLastAdded();
    QCOMPARE(repo.m_shapes.size(), 1);
    QCOMPARE(shape->Type(), type);
    QCOMPARE(shape->Color(), color);
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestRepository::testGetAllShapes() {
    Repository repo;
    //ADD SHAPE1
    Shape::ShapeTypes type1 = Shape::ShapeTypes::Rectangle;
    QString color1("blue");
    Coordinates coords1;
    coords1.topLeftCoordinateX = 1;
    coords1.topLeftCoordinateY = 2;
    coords1.bottomRightCoordinateX = 3;
    coords1.bottomRightCoordinateY = 4;
    repo.addShape(type1,color1,coords1);

    Shape::ShapeTypes type2 = Shape::ShapeTypes::Rectangle;
    QString color2("blue");
    Coordinates coords2;
    coords2.topLeftCoordinateX = 1;
    coords2.topLeftCoordinateY = 2;
    coords2.bottomRightCoordinateX = 3;
    coords2.bottomRightCoordinateY = 4;
    repo.addShape(type2,color2,coords2);

    QList<const Shape*> shapes = repo.getAllShapes();
    const Shape* shape = shapes[0];
    QCOMPARE(shape->Type(), type1);
    QCOMPARE(shape->Color(), color1);
    QCOMPARE(shape->StartCoordinateX(), coords1.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords1.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords1.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords1.bottomRightCoordinateY);

    shape = shapes[1];
    QCOMPARE(shape->Type(), type2);
    QCOMPARE(shape->Color(), color2);
    QCOMPARE(shape->StartCoordinateX(), coords2.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords2.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords2.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords2.bottomRightCoordinateY);


}

QTEST_MAIN(TestRepository)
