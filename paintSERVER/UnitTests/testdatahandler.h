#pragma once
#include <QtTest/QTest>
#include <QObject>
#include "../datahandler.h"
class TestDataHandler : public QObject {
    Q_OBJECT
private slots:
    void testEncodeWithoutText();
    void testDecodeWithoutText();

    void testEncodeWithText();
    void testDecodeWithText();
};

