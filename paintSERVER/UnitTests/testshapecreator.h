#pragma once
#include <QObject>
#include <QtTest>
#include "../Controller/shapecreator.h"
class TestShapeCreator : public QObject {
    Q_OBJECT
private slots:
    void testCreateRectangle();
    void testCreateSquare();
    void testCreateCircle();
    void testCreateLine();
    void testCreatePoint();
    void testCreateText();
};

