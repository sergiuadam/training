#pragma once
#include <QtTest>

class TestRepository : public QObject {
        Q_OBJECT
private slots:
    void testAddShapeNonText();
    void testAddShapeText();
    void testGetLastShapeAdded();
    void testGetAllShapes();
};

