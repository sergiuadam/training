#include "testshapecreator.h"

void TestShapeCreator::testCreateRectangle() {
    ShapeCreator creator;
    Coordinates coords;
    Shape* shape = creator.createShape(Shape::ShapeTypes::Rectangle, QString("red"), coords);

    QCOMPARE(shape->Type(), Shape::ShapeTypes::Rectangle);
    QCOMPARE(shape->Color(), QString("red"));
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestShapeCreator::testCreateSquare() {
    ShapeCreator creator;
    Coordinates coords;
    Shape* shape = creator.createShape(Shape::ShapeTypes::Square, QString("red"), coords);

    QCOMPARE(shape->Type(), Shape::ShapeTypes::Square);
    QCOMPARE(shape->Color(), QString("red"));
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestShapeCreator::testCreateCircle() {
    ShapeCreator creator;
    Coordinates coords;
    Shape* shape = creator.createShape(Shape::ShapeTypes::Circle, QString("red"), coords);

    QCOMPARE(shape->Type(), Shape::ShapeTypes::Circle);
    QCOMPARE(shape->Color(), QString("red"));
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestShapeCreator::testCreateLine() {
    ShapeCreator creator;
    Coordinates coords;
    Shape* shape = creator.createShape(Shape::ShapeTypes::Line, QString("red"), coords);

    QCOMPARE(shape->Type(), Shape::ShapeTypes::Line);
    QCOMPARE(shape->Color(), QString("red"));
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestShapeCreator::testCreatePoint() {
    ShapeCreator creator;
    Coordinates coords;
    Shape* shape = creator.createShape(Shape::ShapeTypes::Point, QString("red"), coords);

    QCOMPARE(shape->Type(), Shape::ShapeTypes::Point);
    QCOMPARE(shape->Color(), QString("red"));
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
}

void TestShapeCreator::testCreateText() {
    ShapeCreator creator;
    Coordinates coords;
    Shape* shape = creator.createShape(QString("red"), coords, QString("TeXt"));

    QCOMPARE(shape->Color(), QString("red"));
    QCOMPARE(shape->StartCoordinateX(), coords.topLeftCoordinateX);
    QCOMPARE(shape->StartCoordinateY(), coords.topLeftCoordinateY);
    QCOMPARE(shape->EndCoordinateX(), coords.bottomRightCoordinateX);
    QCOMPARE(shape->EndCoordinateY(), coords.bottomRightCoordinateY);
    QCOMPARE(shape->Txt(),QString("TeXt"));
}

QTEST_MAIN(TestShapeCreator)
