#include "clienthandler.h"
#include <windows.h>
#include <iostream>
#include <QCoreApplication>
ClientHandler::ClientHandler()= default;

void ClientHandler::startCommunicating(Communication* communication, Controller* controller, const SOCKET& clientSocket, const SOCKET& clientNotifySocket) {
    this->moveToThread(QCoreApplication::instance()->thread());
    this->communication = communication;
    this->controller = controller;
    this->clientSocket = clientSocket;
    this->clientNotifySocket = clientNotifySocket;
    QObject::connect(controller, &Controller::signalShapeAdded, this, &ClientHandler::notifyClient);
    start();
}

void ClientHandler::initializeClientRepository() {
    for (auto iter : controller->getAllShapes()) {
        auto shapeBinary = new QByteArray();
        Coordinates coords;
        coords.topLeftCoordinateX = iter->StartCoordinateX();
        coords.topLeftCoordinateY = iter->StartCoordinateY();
        coords.bottomRightCoordinateX = iter->EndCoordinateX();
        coords.bottomRightCoordinateY = iter->EndCoordinateY();
        int length = dataHandler.enconde(*shapeBinary,iter->Type(), iter->Color(), coords, iter->Txt());
        int res = send(clientNotifySocket,*shapeBinary,length+static_cast<int>(sizeof(int)),0);
        if(res != length + static_cast<int>(sizeof(int))){
            throw QException();
        }
        delete shapeBinary;
        Sleep(SLEEP_TIME_MS); //Weak synchronization technique applied.
    }
}
void ClientHandler::run() {
    int iResult{};
    initializeClientRepository();
    do {
        iResult = recv(clientSocket, static_cast<char*>(recvbuf), recvbuflen, 0);
        if (iResult > 0) {
            QByteArray receivedData = QByteArray::fromRawData(static_cast<char*>(recvbuf),recvbuflen);
            addNewShape(receivedData);
        }
        else {
            closesocket(clientSocket);
            return;
        }
    } while (iResult > 0);
    shutdownClientConnection();
}
void ClientHandler::addNewShape(QByteArray& receivedData) {
    Shape::ShapeTypes shapeType;
    QString color;
    Coordinates coords;
    QString text;
    dataHandler.decode(receivedData,shapeType,color,coords,text);

    if (text == "") {
        controller->addNewShape(shapeType,color,coords);
    }
    else {
        controller->addNewShape(color,coords,text);
    }
}

void ClientHandler::notifyClient(Shape::ShapeTypes type, QString color, Coordinates coords, QString text) {
    QByteArray data;
    dataHandler.enconde(data,type, color, coords,text);
    int arraylength = data.length();
    send(clientNotifySocket,data,arraylength,0);
}

void ClientHandler::shutdownClientConnection() {
    int iResult = shutdown(clientSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        cout<<"shutdown failed with error: "<< WSAGetLastError()<<std::endl;
        closesocket(clientSocket);
        exit(0);
    }
    closesocket(clientSocket);
}
