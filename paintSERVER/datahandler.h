#ifndef DATAHANDLER_H
#define DATAHANDLER_H
#include <QByteArray>
#include <QBuffer>
#include <QDataStream>
#include "Model/shape.h"
class DataHandler {
public:
    DataHandler();
    int enconde(QByteArray& destination, const Shape::ShapeTypes &shapeType, const QString &shapeColor, const Coordinates &coords, const QString& text);
    int decode(QByteArray& source, Shape::ShapeTypes& shapeType, QString& shapeColor, Coordinates& coords, QString& text);
};

#endif // DATAHANDLER_H
