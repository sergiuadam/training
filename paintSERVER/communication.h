#pragma once
#include <iostream>
#include <QVector>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <mutex>
#include <thread>
#include <clienthandler.h>
#define RECEIVE_PORT "20123"
#define SEND_PORT "19999"
#define DEFAULT_BUFLEN 512

class ClientHandler;
class Communication {
private:
    struct addrinfo *result = nullptr, *ptr = nullptr, hints{};
    struct addrinfo *result_notify = nullptr, *ptr_notify = nullptr, hints_notify{};
    int iSendResult{} , clientNumber = 0;
    WSADATA wsaData{};
    SOCKET ListenSocket = INVALID_SOCKET;
    SOCKET NotifySocket = INVALID_SOCKET;
    Controller controller;
    QList<ClientHandler* > clientHandlers;

    void initializeWinsock();
    void resolveAddressAndPort();
    void setupAddrinfo();
    void setupListening();
    void bindTCPSocket();
    void startListening();
    [[noreturn]] void acceptClients();
    void handleClientCommunication(SOCKET& ClientSocket, SOCKET& ClientNotifySocket);

public:
    Communication();
    ~Communication();

    void runServer();
    [[noreturn]] void clientThread(int param);
};
