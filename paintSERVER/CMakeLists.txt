cmake_minimum_required(VERSION 3.5)

project(paintSERVER LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5Core)
find_package(Qt5 REQUIRED Qml Quick Gui)

add_subdirectory("UnitTests")

add_executable(paintSERVER
    main.cpp

    communication.h
    communication.cpp

    clienthandler.h
    clienthandler.cpp

    datahandler.h
    datahandler.cpp

    BackEnd/qqmlobjectlistmodel.h
    BackEnd/qqmlobjectlistmodel.cpp

    Controller/controller.h
    Controller/controller.cpp

    Controller/repository.h
    Controller/repository.cpp

    Controller/inputprocessor.h
    Controller/inputprocessor.cpp

    Controller/mathprocessor.h
    Controller/mathprocessor.cpp

    Controller/shapecreator.h
    Controller/shapecreator.cpp

    Model/shape.h
    Model/rectangle.h
    Model/square.h
    Model/circle.h
    Model/point.h
    Model/line.h
    Model/text.h
    Model/shape.cpp
    Model/rectangle.cpp
    Model/square.cpp
    Model/circle.cpp
    Model/point.cpp
    Model/line.cpp
    Model/text.cpp
)
add_library(paintSERVERLib SHARED
    main.cpp

    communication.h
    communication.cpp

    clienthandler.h
    clienthandler.cpp

    datahandler.h
    datahandler.cpp

    BackEnd/qqmlobjectlistmodel.h
    BackEnd/qqmlobjectlistmodel.cpp

    Controller/controller.h
    Controller/controller.cpp

    Controller/repository.h
    Controller/repository.cpp

    Controller/inputprocessor.h
    Controller/inputprocessor.cpp

    Controller/mathprocessor.h
    Controller/mathprocessor.cpp

    Controller/shapecreator.h
    Controller/shapecreator.cpp

    Model/shape.h
    Model/rectangle.h
    Model/square.h
    Model/circle.h
    Model/point.h
    Model/line.h
    Model/text.h
    Model/shape.cpp
    Model/rectangle.cpp
    Model/square.cpp
    Model/circle.cpp
    Model/point.cpp
    Model/line.cpp
    Model/text.cpp
)
target_link_libraries(paintSERVER Qt5::Core Qt5::Qml Qt5::Quick wsock32 ws2_32)
target_link_libraries(paintSERVERLib Qt5::Core Qt5::Qml Qt5::Quick wsock32 ws2_32)
