#include "communication.h"
#include <QThread>
#include <QException>


Communication::Communication() {
    initializeWinsock();
    setupAddrinfo();
    resolveAddressAndPort();
    setupListening();
    bindTCPSocket();
    freeaddrinfo(result);
}

void Communication::initializeWinsock() {
    int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        std::cout << "WSAStartup failed with error: %d\n" << iResult << std::endl;
        exit(0);
    }
}

void Communication::resolveAddressAndPort() {
    int iResult = getaddrinfo(nullptr, RECEIVE_PORT, &hints, &result);
    if (iResult != 0) {
        std::cout << "getaddrinfo failed:" << iResult << std::endl;
        WSACleanup();
        exit(0);
    }
    iResult = getaddrinfo(nullptr, SEND_PORT, &hints_notify, &result_notify);
    if (iResult != 0) {
        std::cout << "getaddrinfo failed:" << iResult << std::endl;
        WSACleanup();
        exit(0);
    }
}

void Communication::setupAddrinfo() {
    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    ZeroMemory(&hints_notify, sizeof (hints_notify));
    hints_notify.ai_family = AF_INET;
    hints_notify.ai_socktype = SOCK_STREAM;
    hints_notify.ai_protocol = IPPROTO_TCP;
    hints_notify.ai_flags = AI_PASSIVE;
}

void Communication::setupListening() {
    //Setup listening
    ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (ListenSocket == INVALID_SOCKET) {
        std::cout << "socket failed with error: %ld\n"<<WSAGetLastError() << std::endl;
        freeaddrinfo(result);
        WSACleanup();
        exit(0);
    }
    NotifySocket = socket(result_notify->ai_family, result_notify->ai_socktype, result_notify->ai_protocol);
    if (NotifySocket == INVALID_SOCKET) {
        std::cout << "socket failed with error: %ld\n"<<WSAGetLastError() << std::endl;
        freeaddrinfo(result);
        WSACleanup();
        exit(0);
    }
}

void Communication::bindTCPSocket() {
    // Setup the TCP listening socket
    int iResult;
    iResult = bind(ListenSocket, result->ai_addr, static_cast<int>(result->ai_addrlen));
    if (iResult == SOCKET_ERROR) {
        std::cout << "bind failed with error: %d\n"<< WSAGetLastError() << std::endl;
        freeaddrinfo(result);
        closesocket(ListenSocket);
        WSACleanup();
        exit(0);
    }
    iResult = bind(NotifySocket, result_notify->ai_addr, static_cast<int>(result_notify->ai_addrlen));
    if (iResult == SOCKET_ERROR) {
        std::cout << "bind failed with error: %d \n"<< WSAGetLastError() << std::endl;
        freeaddrinfo(result);
        closesocket(NotifySocket);
        WSACleanup();
        exit(0);
    }
}

void Communication::runServer() {

    this->startListening();
    auto acceptClientBind = std::bind(&Communication::acceptClients, this);
    QThread* acceptClientThread = QThread::create(acceptClientBind);
    acceptClientThread->start();
}

void Communication::startListening() {
    int iResult = listen(ListenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR) {
        closesocket(ListenSocket);
        WSACleanup();
        exit(0);
    }
}

[[noreturn]]void Communication::acceptClients() {
    while(true){
        SOCKET ClientSocket = accept(ListenSocket,nullptr,nullptr);
        if (ClientSocket == INVALID_SOCKET) {
            closesocket(ListenSocket);
            WSACleanup();
            ExitThread(0);
        }
        listen(NotifySocket, SOMAXCONN);
        SOCKET ClientNotifySocket = accept(NotifySocket, nullptr, nullptr);
        handleClientCommunication(ClientSocket, ClientNotifySocket);
    }
}

void Communication::handleClientCommunication(SOCKET& ClientSocket, SOCKET& ClientNotifySocket) {
    auto* client = new ClientHandler();
    clientHandlers.append(client);
    clientHandlers.last()->startCommunicating(this,&controller, ClientSocket, ClientNotifySocket);
}

Communication::~Communication() {
    for (auto i : clientHandlers) {
        delete i;
    }
}
