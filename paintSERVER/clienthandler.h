#pragma once
#include <QThread>
#include <winsock2.h>
#include "datahandler.h"
#include "Controller/controller.h"
#include "communication.h"
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>
#define DEFAULT_BUFLEN 512
#define SLEEP_TIME_MS 500
class Communication;
class ClientHandler : public QThread {
    Q_OBJECT
private:

    SOCKET clientSocket{};
    SOCKET clientNotifySocket{};
    char recvbuf[DEFAULT_BUFLEN]{};
    int recvbuflen = DEFAULT_BUFLEN;
    Controller* controller{};
    DataHandler dataHandler{};
    Communication* communication{};

    void initializeClientRepository();
    void addNewShape (QByteArray& receivedData);
    void shutdownClientConnection();
protected:
    void run() override;
public:
    ClientHandler();
    void startCommunicating(Communication* communication, Controller* controller, const SOCKET& clientSocket, const SOCKET& clientNotifySocket);
    void sendToClient();
    bool isInputValid();
public slots:
   void notifyClient(Shape::ShapeTypes type, QString color, Coordinates coords, QString text);
};

