#include <iostream>
#include "communication.h"
#include "Controller/repository.h"
#include <QCoreApplication>
using namespace std;

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);
    qRegisterMetaType<Shape::ShapeTypes>("Shape::ShapeTypes");
    qRegisterMetaType<Coordinates>("Coordinates");
    Communication comm;
    comm.runServer();
    return QCoreApplication::exec();
}
