#include "controller.h"
#include <QThread>

using namespace std;
Controller::Controller()=default;


void Controller::addNewShape(const Shape::ShapeTypes type, const QString& color, Coordinates& coordinates) {
    try {
        m_shapeRepository.addShape(type,color,coordinates);
        emit signalShapeAdded(type,color,coordinates,"");
    } catch(...) {
        throw QException();
    }
}

void Controller::addNewShape(const QString& color, Coordinates& coordinates, const QString& text) {
    try {
        m_shapeRepository.addShape(color, coordinates, text);
        emit signalShapeAdded(Shape::ShapeTypes::Text,color,coordinates,text);
    } catch(...) {
        throw QException();
    }
}
const Shape* Controller::getLast() {
    return m_shapeRepository.getLastAdded();
}
QList<const Shape*> Controller::getAllShapes() {
    return m_shapeRepository.getAllShapes();
}
