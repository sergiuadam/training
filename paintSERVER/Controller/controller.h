#pragma once
#include <cstring>
#include "repository.h"
#include "excpt.h"
#include "vector"
#include "string"
#include <iostream>
#include <utility>
#include <QException>
using namespace std;

class TestController;
class Controller : public QObject{
friend class TestController;
    Q_OBJECT
private:
    Repository m_shapeRepository;
public:
    Controller();
    void addNewShape(const Shape::ShapeTypes type, const QString& color, Coordinates& coordinates);
    void addNewShape(const QString& color, Coordinates& coordinates, const QString& text);
    const Shape* getLast();
    QList<const Shape*> getAllShapes();
signals:
    void signalShapeAdded(Shape::ShapeTypes type, QString color, Coordinates coords, QString text);
};
