#include "repository.h"
#include <QString>
#include <QVector>
#include <iostream>
#include <utility>
#include <QException>
using namespace std;
Repository::Repository() = default;

void Repository::addShape(Shape::ShapeTypes type, const QString& color, Coordinates coordinates) {
    try {
        Shape* newShape = m_creator.createShape(type, color, coordinates);
        repoMutex.lock();
        m_shapes.append(newShape);
        repoMutex.unlock();
    } catch(...) {
        throw QException();
    }
}

void Repository::addShape(const QString& color, Coordinates coordinates, const QString& Text) {
    try {
        Shape* newShape = m_creator.createShape(color, coordinates, Text);
        m_shapes.append(newShape);

    } catch(...) {
        throw QException();
    }
}
const Shape* Repository::getLastAdded() {
    return m_shapes.last();


}
QList<const Shape *> Repository::getAllShapes() {
    return m_shapes;
}
