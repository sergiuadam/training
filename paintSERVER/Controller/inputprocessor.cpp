#include "inputprocessor.h"
#include <algorithm>
#include "QException"
InputProcessor::InputProcessor() = default;

//If user does not draw from top left to bottom right,
//like a normal human being, then we have to switch the corners' coordinates;
void InputProcessor::arrangeCoordinates(Coordinates& coordinates) {
    if (coordinates.topLeftCoordinateX > coordinates.bottomRightCoordinateX ) {
        std::swap(coordinates.topLeftCoordinateX, coordinates.bottomRightCoordinateX);
    }

    if(coordinates.topLeftCoordinateY > coordinates.bottomRightCoordinateY) {
        std::swap(coordinates.topLeftCoordinateY, coordinates.bottomRightCoordinateY);
    }
}

void InputProcessor::filterNullText(const QString &text) {
    if(text == nullptr || text == ""){
        throw QException();
    }
}
