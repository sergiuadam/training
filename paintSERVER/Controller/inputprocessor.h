#pragma once

#include "../Model/shape.h"
class InputProcessor {

public:
    InputProcessor();
    //If user does not draw from top left to bottom right
    //like a normal human being, then we have to switch the corners;
    void arrangeCoordinates(Coordinates& coordinates);
    void filterNullText(const QString& text);
};
