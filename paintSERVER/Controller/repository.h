#pragma once
#include <qmutex.h>
#include <QVector>
#include <QBuffer>
#include <QDataStream>
#include <string>
#include "../Model/shape.h"
#include "shapecreator.h"

using namespace std;
class Controller;
class Repository {
friend class TestRepository;
private:
    QMutex repoMutex{};
    QList<const Shape*> m_shapes;
    ShapeCreator m_creator;

public:
    Repository();
    void addShape(Shape::ShapeTypes type, const QString& color, Coordinates coordinates);
    void addShape(const QString& color, Coordinates coordinates, const QString& Text);
    const Shape* getLastAdded();
    QList<const Shape*> getAllShapes();
};

