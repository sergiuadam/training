#include "cerebro.h"
#include <iostream>

void Cerebro::registerObserver(Observer* obs) {
    observers.push_back(obs);
    notifyObservers();
    cout<<"Done \n\n";
}

void Cerebro::unregisterObserver(Observer* obs) {
    for(auto it = observers.begin(); it < observers.end(); it++){
        if(*it == obs){
            observers.erase(it);
        }
    }
}

void Cerebro::notifyObservers() {
    for(unsigned long long iter = 0; iter < observers.size(); iter++){
        observers[iter]->notify();
    }
}

Cerebro::Cerebro() {

}

Cerebro::~Cerebro() {
    for(auto iter = setOfMutants.begin(); iter != setOfMutants.end(); iter++){
        delete *iter;
    }
}

Cerebro& Cerebro::operator+=(Mutant * M) {
    setOfMutants.insert(M);
    M->enterRoom(this);
    return *this;
}

Cerebro& Cerebro::operator-=(Mutant * M) {
    for(auto i = setOfMutants.begin(); i != setOfMutants.end(); i++){
        if (*i == M){
            setOfMutants.erase(i);
            M->quitRoom();
        }
    }
    return *this;
}

int Cerebro::getNumberOfEntities() const {
    return static_cast<int>(setOfMutants.size());
}

std::set<Mutant*>::iterator Cerebro::getIterator() const{
    return setOfMutants.begin();
}
