#ifndef CEREBRO_H
#define CEREBRO_H
#include <set>
#include <mutant.h>
#include <observable.h>

class Cerebro : public Observable {

private:
    std::set<Mutant*> setOfMutants;
    int nrOfTelepaths;

public:
    void registerObserver(Observer*) override;
    void unregisterObserver(Observer* ) override;
    void notifyObservers() override;

    Cerebro();
    ~Cerebro() override;

    Cerebro& operator+=(Mutant*);
    Cerebro& operator-=(Mutant*);

    int getNumberOfEntities() const;
    std::set<Mutant*>::iterator getIterator() const;


};

#endif // CEREBRO_H
