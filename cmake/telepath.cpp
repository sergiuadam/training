#include <iostream>
#include "telepath.h"
#include "cerebro.h"
using namespace std;

Telepath::Telepath() {

}

Telepath::~Telepath() {

}
void Telepath::enterRoom(Cerebro* room) {
    if(myRoom == nullptr){
        myRoom = room;
    }
    else{
        quitRoom();
        myRoom = room;
    }
    myRoom->registerObserver(this);
}
void Telepath::quitRoom(){
    myRoom->unregisterObserver(this);
    myRoom = nullptr;
}
int Telepath::getNumberOfTelepaths() const {
    int res = 0;
    auto iter = myRoom->getIterator();
    for(int i = 0; i < myRoom->getNumberOfEntities(); i++){
        if(dynamic_cast<Telepath*>(*iter)!=nullptr){
            res++;
        }
        iter++;
    }
    return res;
}

void Telepath::notify() {
    //cout<<"Notified \n";
}
