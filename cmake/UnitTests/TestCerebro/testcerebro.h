//#ifndef TESTCEREBRO_H
//#define TESTCEREBRO_H
#include <QtTest>
#include "../../cerebro.h"
#include "../../telepath.h"

class CerebroTest : public QObject
{
    Q_OBJECT
private slots:
    void getNumberOfEntitiesTest();
    void registerObserverTest();
    void unregisterObserverTest();
    void operatorsTest();
};

//#endif // TESTCEREBRO_H
