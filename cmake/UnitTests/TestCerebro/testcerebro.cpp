#include "testcerebro.h"
#include <QtTest>
#include <iostream>
using namespace std;

void CerebroTest::getNumberOfEntitiesTest() {
    Telepath * t1 = new Telepath();
    Cerebro * room = new Cerebro();
    //Private method access;
    QVERIFY(room->returnNumberTen() == 10);
    //Private attribute access;
    QVERIFY(room->setOfMutants.size()==0);


    QVERIFY(room->getNumberOfEntities() == 0);

    *room+=t1;
    QVERIFY(room->getNumberOfEntities() == 1);

    *room-=t1;
    QVERIFY(room->getNumberOfEntities() == 0);

    delete t1;
    delete room;
}
void CerebroTest::registerObserverTest(){
    Telepath * t1 = new Telepath();
    Cerebro * room = new Cerebro();
    QVERIFY(room->observers.size()==0);

    room->registerObserver(t1);
    QVERIFY(room->observers[0] == t1);
    QVERIFY(room->observers.size()==1);
    room->unregisterObserver(t1);
    delete t1;
    delete room;
}

void CerebroTest::unregisterObserverTest() {
    Telepath * t1 = new Telepath();
    Cerebro * room = new Cerebro();
    QVERIFY(room->observers.size()==0);

    room->registerObserver(t1);
    QVERIFY(room->observers[0] == t1);
    QVERIFY(room->observers.size()==1);

    room->unregisterObserver(t1);
    QVERIFY(room->observers.size()==0);
    delete t1;
    delete room;
}

void CerebroTest::operatorsTest() {
    Telepath * t1 = new Telepath();
    Telepath * t2 = new Telepath();
    Cerebro * room = new Cerebro();
    QVERIFY(room->getNumberOfEntities() == 0);

    *room+=t1;
    QVERIFY(*room->getIterator()==t1);
    QVERIFY(room->getNumberOfEntities() == 1);

    *room-=t2;
    QVERIFY(*room->getIterator()==t1);
    QVERIFY(room->getNumberOfEntities() == 1);

    *room-=t1;
    *room+=t2;
    QVERIFY(*room->getIterator()==t2);
    QVERIFY(room->getNumberOfEntities() == 1);

    *room-=t2;
    QVERIFY(*room->getIterator()!=t1);
    QVERIFY(room->getNumberOfEntities() == 0);
    delete t1;
    delete t2;
    delete room;
}

QTEST_MAIN(CerebroTest)

