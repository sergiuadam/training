#include "testtelepath.h"
#include <QtTest>
#include <QScopedPointer>
#include <iostream>
#include <memory>
using namespace std;



void TelepathTest::testEnterRoom() {
    Cerebro* room = new Cerebro();
    Cerebro* room2 = new Cerebro();
    Telepath * T = new Telepath();
    T->enterRoom(room);
    QVERIFY(T->myRoom == room);
    T->enterRoom(room2);
    QVERIFY(T->myRoom == room2);

    delete room;
    delete room2;
    delete T;
}

void TelepathTest::testQuitRoom() {
    Telepath * T = new Telepath();
    std::unique_ptr<Cerebro>room(new Cerebro());
    T->enterRoom(&*room);
    T->quitRoom();
    QVERIFY(T->myRoom == nullptr);

    delete T;
}

void TelepathTest::testGetNumberOfTelepaths() {
    QScopedArrayPointer<Telepath> t (new Telepath[10]);
    Elemental * e1 = new Elemental();
    TelepathTelekinetic * ttk1 = new TelepathTelekinetic();
    Telekinetic * tk2= new Telekinetic();
    Cerebro* room = new Cerebro();

    *room+=e1;
    *room+=ttk1;
    *room+=tk2;

    for(int i = 0; i < 10; i++){
        *room+=&t[i];
        if(dynamic_cast<Telepath*>(&t[i])){
            QVERIFY(t[i].getNumberOfTelepaths() == i+2);
        }
    }
}

QTEST_MAIN(TelepathTest);
