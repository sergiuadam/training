//#ifndef TESTCEREBRO_H
//#define TESTCEREBRO_H
#include <QtTest>
#include "../../cerebro.h"
#include "../../telepath.h"
#include "../../elemental.h"
#include "../../telekinetic.h"
#include "../../telepathtelekinetic.h"

class TelepathTest : public QObject{
    Q_OBJECT
private slots:
    void testEnterRoom();
    void testQuitRoom();
    void testGetNumberOfTelepaths();
};

//#endif // TESTCEREBRO_H
