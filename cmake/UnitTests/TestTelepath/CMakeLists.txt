cmake_minimum_required(VERSION 3.10)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
project(TelepathTest)
set(TelepathTestSources
    testtelepath.h
    testtelepath.cpp
    ../../cerebro.h
    ../../elemental.h
    ../../mutant.h
    ../../observable.h
    ../../observer.h
    ../../telekinetic.h
    ../../telepath.h
    ../../telepathtelekinetic.h
    ../../cerebro.cpp
    ../../elemental.cpp
    ../../mutant.cpp
    ../../observable.cpp
    ../../observer.cpp
    ../../telekinetic.cpp
    ../../telepath.cpp
    ../../telepathtelekinetic.cpp
    )
set(CMAKE_AUTOMOC ON)
add_executable(TelepathTest "${TelepathTestSources}")
find_package(Qt5 COMPONENTS Test REQUIRED)
target_link_libraries(TelepathTest Qt5::Test)

