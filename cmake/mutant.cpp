#include "mutant.h"
#include "cerebro.h"
#include <iostream>
using namespace std;
Mutant::Mutant(){
  myRoom = nullptr;

}
Mutant::Mutant(int x){

}
Mutant::~Mutant(){

}
void Mutant::enterRoom(Cerebro* room){
    if(myRoom == nullptr){
        myRoom = room;
    }
    else{
        quitRoom();
        myRoom = room;
    }
}
void Mutant::quitRoom(){
    if(myRoom != nullptr){
        myRoom = nullptr;
    }
}
