#include <iostream>
#include <vector>
#include "cerebro.h"
#include "telepath.h"
#include "elemental.h"
#include "telepathtelekinetic.h"
using namespace std;

int main() {
   Cerebro* room = new Cerebro();
   Telepath* t1 = new Telepath();
   Telepath* t2 = new Telepath();
   *room+=t1;
   *room+=t2;
   //*room-=t1;
   t1->notify();
   t2->notify();
   return 0;
}
