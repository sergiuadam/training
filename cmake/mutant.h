#ifndef MUTANT_H
#define MUTANT_H
#include <iostream>
class Cerebro;

class Mutant {
protected:
    Cerebro* myRoom;

public:
    Mutant();
    Mutant(int x);
    virtual void enterRoom(Cerebro* );
    virtual void quitRoom();

    void TelekineticSpecific();

    virtual ~Mutant();
};

#endif // MUTANT_H
