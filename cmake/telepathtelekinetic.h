#ifndef TELEPATHTELEKINETIC_H
#define TELEPATHTELEKINETIC_H
#include "telepath.h"
#include "telekinetic.h"

class TelepathTelekinetic : public Telepath, public Telekinetic {
public:
    TelepathTelekinetic();
    TelepathTelekinetic(int x);
    ~TelepathTelekinetic() override;
};

#endif // TELEPATHTELEKINETIC_H
