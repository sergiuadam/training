#ifndef OBSERVABLE_H
#define OBSERVABLE_H
#include <iostream>
#include <vector>
#include "observer.h"

using namespace std;

class Observable
{
public:
    vector<Observer*> observers;
    Observable();
    virtual ~Observable();
    virtual void registerObserver(Observer* )=0;
    virtual void unregisterObserver(Observer* )=0;
    virtual void notifyObservers()=0;
};

#endif // OBSERVABLE_H
