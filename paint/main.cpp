// main.cpp
#include <QGuiApplication>
#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include "BackEnd/backend.h"
#include "Controller/controller.h"
#include "Model/rectangle.h"
int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);
    app.setOrganizationName("FLORIN");
    app.setOrganizationDomain("SALAM");
    qmlRegisterType<BackEnd>("cpp_backend",1,0,"Backend");
    qmlRegisterUncreatableType<Shape>("cpp_shape",1,0,"Cppshape","Interface");
    qmlRegisterType<ShapeModel>("cpp_shapemodel",1,0,"CppModel");
    qRegisterMetaType<Coordinates>();
    qRegisterMetaType<Shape::ShapeTypes>();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/view.qml")));
    return QGuiApplication::exec();

}
