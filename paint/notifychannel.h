#ifndef NOTIFYCHANNEL_H
#define NOTIFYCHANNEL_H
#include <ws2tcpip.h>
#include <winsock2.h>
#include <QThread>
#include <QBuffer>
#include <QDataStream>
#include "Controller/repository.h"
#define DEFAULT_BUFLEN 512

class NotifyChannel : public QThread{
    Q_OBJECT
private:
    SOCKET notifySocket;
    char recvbuf[DEFAULT_BUFLEN]{};
    int recvbuflen = DEFAULT_BUFLEN;
protected:
    void run() override;
public:
    NotifyChannel(SOCKET& notifySocket);
    void decodeAndCreate(QByteArray& receivedData);

signals:
     void shapeReceived(Shape::ShapeTypes shapeType, QString color,Coordinates coords);
     void shapeReceivedText(QString color,Coordinates coords,QString text);
};

#endif // NOTIFYCHANNEL_H
