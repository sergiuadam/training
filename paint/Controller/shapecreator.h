#pragma once

#include "../Model/rectangle.h"
#include "../Model/square.h"
#include "../Model/circle.h"
#include "../Model/point.h"
#include "../Model/line.h"
#include "../Model/text.h"
#include "inputprocessor.h"
#include "mathprocessor.h"

class ShapeCreator {
private:
    InputProcessor m_inputProcessor;

public:
    ShapeCreator();
    Shape* createShape(Shape::ShapeTypes type, const QString& color, Coordinates coordinates);
    Shape* createShape(const QString& color, Coordinates coordinates, const QString& Text);
};
