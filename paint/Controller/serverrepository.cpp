#include "serverrepository.h"
#include <time.h>
ServerRepository::ServerRepository() = default;

SOCKET ServerRepository::connectToServer(){
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);

    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo("192.168.230.101", DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();

    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != nullptr ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();

        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen));
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }
    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    getaddrinfo("192.168.230.101", "19999", &hints, &result);
    for(ptr=result; ptr != nullptr ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        NotifySocket = socket(ptr->ai_family, ptr->ai_socktype,
                               ptr->ai_protocol);
        if (NotifySocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();

        }

        // Connect to server.
        iResult = connect( NotifySocket, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen));
        if (iResult == SOCKET_ERROR) {
            closesocket(NotifySocket);
            NotifySocket = INVALID_SOCKET;
            continue;
        }
        break;
    }
    return NotifySocket;
}

void ServerRepository::addShape(Shape::ShapeTypes types, const QString& color, Coordinates coords) {
    QByteArray data;
    QBuffer buffer(&data);
    buffer.open(QBuffer::ReadWrite);
    QDataStream out(&buffer);

    out << static_cast<quint32>(types);
    out << color;
    out << static_cast<quint32>(coords.topLeftCoordinateX);
    out << static_cast<quint32>(coords.topLeftCoordinateY);
    out << static_cast<quint32>(coords.bottomRightCoordinateX);
    out << static_cast<quint32>(coords.bottomRightCoordinateY);

    out << static_cast<quint32>(buffer.data().length());

    iResult = send(ConnectSocket, buffer.data(), buffer.data().length(), 0 );
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    printf("Bytes Sent: %ld\n", iResult);
}

void ServerRepository::addShapeText (const QString &color, Coordinates coords, const QString &Text){
    QByteArray data;
    QBuffer buffer(&data);
    buffer.open(QBuffer::ReadWrite);
    QDataStream out(&buffer);

    if (Text == ""){
        return;
    }
    Shape::ShapeTypes type = Shape::ShapeTypes::Text;
    out << static_cast<quint32>(type);
    out << color;
    out << static_cast<quint32>(coords.topLeftCoordinateX);
    out << static_cast<quint32>(coords.topLeftCoordinateY);
    out << static_cast<quint32>(coords.bottomRightCoordinateX);
    out << static_cast<quint32>(coords.bottomRightCoordinateY);
    out << Text;
    out << static_cast<quint32>(buffer.data().length());

    iResult = send(ConnectSocket, buffer.data(), buffer.data().length(), 0 );
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
    }

    printf("Bytes Sent: %ld\n", iResult);
}
