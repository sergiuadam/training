#pragma once
#include <QQmlApplicationEngine>
#include <cstring>
#include <iostream>
#include <utility>
#include <QException>
#include "filehandler.h"
#include "repositoryinterface.h"
#include "repository.h"
#include "serverrepository.h"
#include "excpt.h"
#include "vector"
#include "string"
#include "notifychannel.h"
using namespace std;
class Controller : public QObject{
    Q_OBJECT
private:
    FileHandler m_fileHandler;
    Repository m_shapeRepository;
    ServerRepository m_server;
    NotifyChannel* channel;
public:
    Controller();
    void addNewShape(const Shape::ShapeTypes type, const QString& color, Coordinates& coordinates);
    void addNewShape(const QString& color, Coordinates& coordinates, const QString& text);
    void saveToFile(const QString& path);
    void loadFromFile(const QString& path);
    ShapeModel* getAllShapes();
};
