#include "controller.h"
using namespace std;

Controller::Controller(){
    SOCKET notifySocket = m_server.connectToServer();
    NotifyChannel* channel = new NotifyChannel(notifySocket);
    QObject::connect(channel, &NotifyChannel::shapeReceived, &m_shapeRepository, &Repository::addShape);
    QObject::connect(channel, &NotifyChannel::shapeReceivedText, &m_shapeRepository, &Repository::addShapeText);
    channel->start();
}

void Controller::addNewShape(const Shape::ShapeTypes type, const QString& color, Coordinates& coordinates) {
    try{
        m_server.addShape(type, color, coordinates);
    }catch(...){
        throw QException();
    }
}

void Controller::addNewShape(const QString& color, Coordinates& coordinates, const QString& text) {
    try{
        m_server.addShapeText(color, coordinates, text);
    }catch(...){
        throw QException();
    }
}

void Controller::saveToFile(const QString& path) {
    m_fileHandler.writeToFile(m_shapeRepository, path);
}

void Controller::loadFromFile(const QString& path) {
    try{
        m_fileHandler.loadFromFile(m_shapeRepository, path);
    }
    catch(...){
        throw QException();
    }
}

ShapeModel* Controller::getAllShapes() {
    return m_shapeRepository.getAllShapes();
}
