#pragma once
#include "repositoryinterface.h"
using namespace std;

class Repository : public QObject{
    Q_OBJECT
friend class TestRepository;

private:
    ShapeModel* m_shapes;
    ShapeCreator m_creator;

public:
    Repository();
    ShapeModel* getAllShapes();

public slots:
    void addShape(Shape::ShapeTypes shapeType,const QString& color, Coordinates coordinates);
    void addShapeText(const QString& color, Coordinates coordinates, const QString& text);

};

