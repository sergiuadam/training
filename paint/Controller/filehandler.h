#pragma once
#include "map"
#include "string"
#include "vector"
#include "../Model/shapemodel.h"
#include "repository.h"
#include <Qt>
#include <LIBXL/include_cpp/libxl.h>
using namespace std;

class FileHandler {
public:
    FileHandler();
    void writeToFile(Repository& repo, const QString& path);
    void loadFromFile(Repository& repo, const QString& path);

    void readData(const libxl::Book* book, Repository& repo);
};

