#include "shapecreator.h"

#include <utility>
#include "../Model/shape.h"
#include <QException>
ShapeCreator::ShapeCreator() = default;

Shape* ShapeCreator::createShape(Shape::ShapeTypes type, const QString& color, Coordinates coordinates) {
    m_inputProcessor.arrangeCoordinates(coordinates);
    switch(type) {
    case Shape::ShapeTypes::Rectangle : {
        return new Rectangle(color, coordinates);
    }
    case Shape::ShapeTypes::Square : {
        return new Square(color,coordinates);
    }
    case Shape::ShapeTypes::Circle : {
        return new Circle(color, coordinates);
    }
    case Shape::ShapeTypes::Line : {
        return new Line(color, coordinates);
    }
    case Shape::ShapeTypes::Point : {
        return new Point(color, coordinates);
    }
    default: throw QException();
    }
}

Shape* ShapeCreator::createShape(const QString& color, Coordinates coordinates, const QString& text) {
    try{
        m_inputProcessor.filterNullText(text);
    } catch (...) {
        throw QException();
    }
    return new Text(color, coordinates, text);
}
