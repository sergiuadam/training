#include "mathprocessor.h"

MathProcessor::MathProcessor() = default;

void MathProcessor::computeRectangleWidth(const Coordinates &coordinates, int &width) {
    width = coordinates.bottomRightCoordinateX - coordinates.topLeftCoordinateX;
}

void MathProcessor::computeRectangleHeight(const Coordinates& coordinates, int &height) {
    height = coordinates.bottomRightCoordinateY - coordinates.topLeftCoordinateY;
}

void MathProcessor::computeSquare(const Coordinates& coordinates, int &sideLength) {
    if (coordinates.bottomRightCoordinateX - coordinates.topLeftCoordinateX < coordinates.bottomRightCoordinateY - coordinates.topLeftCoordinateY){
        sideLength = coordinates.bottomRightCoordinateX - coordinates.topLeftCoordinateX;
    }
    else {
        sideLength = coordinates.bottomRightCoordinateY - coordinates.bottomRightCoordinateX;
    }
}

void MathProcessor::computeCircle(const Coordinates& coordinates, int &radius) {
    radius = (coordinates.bottomRightCoordinateX - coordinates.topLeftCoordinateX) / 2;
}


