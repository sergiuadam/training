#ifndef SERVERREPOSITORY_H
#define SERVERREPOSITORY_H
#include <ws2tcpip.h>
#include <winsock2.h>
#include <QByteArray>
#include <QDataStream>
#include <QBuffer>
#include <QThread>

#include "repositoryinterface.h"
#define DEFAULT_PORT "20123"
#define DEFAULT_BUFLEN 512

class ServerRepository {
private:
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    SOCKET NotifySocket = INVALID_SOCKET;
    struct addrinfo *result = nullptr, *ptr = nullptr, hints;
    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;
    int iResult;

public:
    ServerRepository();
    SOCKET connectToServer();
    void addShape(Shape::ShapeTypes types, const QString& color, Coordinates coords);
    void addShapeText(const QString& color, Coordinates coords, const QString& Text);
};

#endif // SERVERREPOSITORY_H
