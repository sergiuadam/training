#include "filehandler.h"
#include <QString>
#include <fstream>
#include <iostream>
#include <QException>
#include <LIBXL/include_cpp/libxl.h>

#define TYPE_INDEX 0
#define COLOR_INDEX 1
#define START_COORDX_INDEX 2
#define START_COORDY_INDEX 3
#define END_COORDX_INDEX 4
#define END_COORDY_INDEX 5
#define TEXT_INDEX 6

FileHandler::FileHandler() = default;

/* Production code would've been broken into smaller functions */

void FileHandler::writeToFile(Repository& repo, const QString& path){
    using namespace libxl;

    ShapeModel* allShapes = repo.getAllShapes();
    Book* book = xlCreateXMLBook();
    Sheet* sheet = book->addSheet("Sheet1");

    int row = 1;
    for(int i = 0; i < allShapes->size(); i++) {
        sheet->writeNum(row,TYPE_INDEX,allShapes->at(i)->Type());
        sheet->writeStr(row, COLOR_INDEX, allShapes->at(i)->Color().toStdString().c_str());
        sheet->writeNum(row, START_COORDX_INDEX, allShapes->at(i)->StartCoordinateX());
        sheet->writeNum(row, START_COORDY_INDEX, allShapes->at(i)->StartCoordinateY());
        sheet->writeNum(row, END_COORDX_INDEX, allShapes->at(i)->EndCoordinateX());
        sheet->writeNum(row, END_COORDY_INDEX, allShapes->at(i)->EndCoordinateY());

        if(allShapes->at(i)->Type() == Shape::ShapeTypes::Text) {
            sheet->writeStr(row,TEXT_INDEX,allShapes->at(i)->Txt().toStdString().c_str());
        }
        row++;
    }
    book->save(path.toStdString().c_str());
    book->release();
}

void FileHandler::loadFromFile(Repository& repo,const QString& path) {
    try{
        using namespace libxl;
        Book* book = xlCreateXMLBookA(); //xlsx
        book->load(path.toStdString().c_str());
        readData(book, repo);
    }catch(...){
        throw QException();
    }
}

void FileHandler::readData(const libxl::Book* book, Repository &repo) {
    try{
        using namespace libxl;
        Sheet* sheet = book->getSheet(0);
        if (sheet == nullptr) {
            throw QException();
        }

        const int sheetOffset = 1;
        for(int currentRow = sheet->firstRow() + sheetOffset; currentRow < sheet->lastRow(); currentRow++) {
            int startCoordX; int startCoordY; int endCoordX; int endCoordY;
            Shape::ShapeTypes type;
            QString color; QString text;

            type = static_cast<Shape::ShapeTypes>(sheet->readNum(currentRow,TYPE_INDEX));
            color = static_cast<QString>(sheet->readStr(currentRow,COLOR_INDEX));
            startCoordX = static_cast<int>(sheet->readNum(currentRow,START_COORDX_INDEX));
            startCoordY = static_cast<int>(sheet->readNum(currentRow,START_COORDY_INDEX));
            endCoordX = static_cast<int>(sheet->readNum(currentRow,END_COORDX_INDEX));
            endCoordY = static_cast<int>(sheet->readNum(currentRow,END_COORDY_INDEX));

            if(type == Shape::ShapeTypes::Text){
                text = sheet->readStr(currentRow,TEXT_INDEX);
            }

            Coordinates coords;
            coords.topLeftCoordinateX = startCoordX;
            coords.topLeftCoordinateY = startCoordY;
            coords.bottomRightCoordinateX = endCoordX;
            coords.bottomRightCoordinateY = endCoordY;
            if(text != nullptr){
                repo.addShapeText(color,coords,text);
                text = nullptr;
            }
            else{
                repo.addShape(type,color,coords);
            }
        }
    } catch(...){
        throw QException();
    }
}

