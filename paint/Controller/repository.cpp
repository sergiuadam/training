#include "repository.h"
#include <QString>
#include <QVector>
#include <iostream>
#include <utility>
#include <QException>
using namespace std;


Repository::Repository(){
    m_shapes = new ShapeModel();
}

void Repository::addShape(Shape::ShapeTypes shapeType, const QString& color, Coordinates coordinates){
    try{
        Shape* newShape = m_creator.createShape(shapeType, color, coordinates);
        m_shapes->append(newShape);
    }catch(...){
        throw QException();
    }
}

void Repository::addShapeText(const QString& color, Coordinates coordinates, const QString& Text){
    try{
        Shape* newShape = m_creator.createShape(color, coordinates, Text);
        m_shapes->append(newShape);
    }catch(...){
        throw QException();
    }
}

ShapeModel* Repository::getAllShapes() {
    return m_shapes;
}

