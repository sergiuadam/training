#pragma once
#include "../Model/shape.h"
class MathProcessor {
public:
    MathProcessor();
    void computeRectangleWidth(const Coordinates& coordinates, int& width);
    void computeRectangleHeight(const Coordinates& coordinates, int& height);
    void computeSquare(const Coordinates& coordinates, int& sideLength);
    void computeCircle(const Coordinates& coordinates, int& radius);

};
