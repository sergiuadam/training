#pragma once

#include "shape.h"
#include "../BackEnd/qqmlobjectlistmodel.h"

class ShapeModel : public QQmlObjectListModel<Shape> {
    Q_OBJECT
public:
    ShapeModel();
};
