#pragma once
#include "shape.h"

class Line : public Shape {
public:
    Line(QString color, Coordinates coordinates);
};
