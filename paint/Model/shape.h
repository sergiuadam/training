#pragma once
#include <QObject>
#include <string>
#include <QString>
#include <QStringList>
using namespace std;

struct Coordinates{
    int topLeftCoordinateX{}, bottomRightCoordinateX{};
    int topLeftCoordinateY{}, bottomRightCoordinateY{};
};
Q_DECLARE_METATYPE(Coordinates);

class Shape : public QObject {
    Q_OBJECT

public:
    enum ShapeTypes {Rectangle, Square, Circle, Line, Point, Text, Invalid};
    Q_ENUM(ShapeTypes)

private:
    ShapeTypes m_type;
    QString m_color;
    Coordinates m_coordinates;
    QString m_text;

public:
    Q_PROPERTY(ShapeTypes Type READ Type WRITE setType NOTIFY noNotify)
    Q_PROPERTY(QString Color READ Color WRITE setColor NOTIFY noNotify)
    Q_PROPERTY(int StartCoordinateX READ StartCoordinateX NOTIFY noNotify)
    Q_PROPERTY(int StartCoordinateY READ StartCoordinateY NOTIFY noNotify)
    Q_PROPERTY(int EndCoordinateX READ EndCoordinateX NOTIFY noNotify)
    Q_PROPERTY(int EndCoordinateY READ EndCoordinateY NOTIFY noNotify)
    Q_PROPERTY(QString Txt READ Txt NOTIFY noNotify)

    Shape(QString,Coordinates);
    ~Shape() override;

    void setType(const ShapeTypes type);
    void setColor(QString color);
    void setText(QString text);
    void setCoordinateX(int coordX);
    void setCoordinateY(int coordY);

    ShapeTypes Type() const;
    QString Color() const;
    QString Txt() const;
    int StartCoordinateX() const;
    int StartCoordinateY() const;
    int EndCoordinateX() const;
    int EndCoordinateY() const;
signals:
    void noNotify() const;
};
