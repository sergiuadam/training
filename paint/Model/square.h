#pragma once
#include "shape.h"

class Square : public Shape {
public:
    Square(QString color, Coordinates coordinates);
};

