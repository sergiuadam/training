#include "text.h"

#include <utility>

Text::Text(QString color,Coordinates coordinates, QString text):Shape(std::move(color),coordinates) {
    setType(ShapeTypes::Text);
    setText(std::move(text));
}
