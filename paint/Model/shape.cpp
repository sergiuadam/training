#include "shape.h"

#include <utility>

Shape::Shape(QString color, Coordinates coordinates) {
    m_type = ShapeTypes::Invalid;
    m_color = std::move(color);
    m_coordinates = coordinates;
    m_text = "nulltext";
}

Shape::~Shape() = default;

void Shape::setType(ShapeTypes type) {
    this->m_type = type;
}

void Shape::setColor(QString color) {
    this->m_color = std::move(color);
}

void Shape::setText(QString text) {
    m_text = std::move(text);
}

Shape::ShapeTypes Shape::Type() const {
    return m_type;
}

QString Shape::Color() const {
    return m_color;
}

int Shape::StartCoordinateX() const {
    return m_coordinates.topLeftCoordinateX;
}

int Shape::StartCoordinateY() const {
    return m_coordinates.topLeftCoordinateY;
}

int Shape::EndCoordinateX() const {
    return m_coordinates.bottomRightCoordinateX;
}

int Shape::EndCoordinateY() const {
    return m_coordinates.bottomRightCoordinateY;
}

QString Shape::Txt() const{
    return m_text;
}
