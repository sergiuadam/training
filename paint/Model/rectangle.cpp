#include "rectangle.h"

#include <utility>

Rectangle::Rectangle(QString color, Coordinates coordinates) : Shape(std::move(color), coordinates) {
    setType(ShapeTypes::Rectangle);
}

