#include "testinputprocessor.h"
#include <QtTest>

void TestInputProcessor::testArrangeCoordinates() {
    InputProcessor inputProcessor;
    int coordX1;
    int coordX2;
    int coordY1;
    int coordY2;

    //CASE 1: DRAGS FROM TOP LEFT CORNER TO BOTTOM RIGHT CORNER
    //x1 < x2; y1 < y2;
    coordX1 = 0;
    coordX2 = 100;
    coordY1 = 0;
    coordY2 = 100;
    inputProcessor.arrangeCoordinates(coordX1, coordY1, coordX2, coordY2);
    QVERIFY((coordX1 < coordX2) && (coordY1 < coordY2));

    //CASE 2: DRAGS FROM TOP RIGHT CORNER TO BOTTOM LEFT CORNER
    //X1 > X2, Y1 < Y2
    coordX1 = 100;
    coordX2 = 0;
    coordY1 = 0;
    coordY2 = 100;
    inputProcessor.arrangeCoordinates(coordX1, coordY1, coordX2, coordY2);
    QVERIFY((coordX1 < coordX2) && (coordY1 < coordY2));

    //CASE 3: DRAGS FROM BOTTOM LEFT CORNER TO TOP RIGHT CORNER
    // X1 < X2, Y1 > Y2
    coordX1 = 0;
    coordX2 = 100;
    coordY1 = 100;
    coordY2 = 0;
    inputProcessor.arrangeCoordinates(coordX1, coordY1, coordX2, coordY2);
    QVERIFY((coordX1 < coordX2) && (coordY1 < coordY2));

    //CASE 4: DRAGS FROM BOTTOM RIGHT CORNER TO TOP LEFT CORNER
    //X1 > X2, Y1 > Y2
    coordX1 = 100;
    coordX2 = 0;
    coordY1 = 100;
    coordY2 = 0;
    inputProcessor.arrangeCoordinates(coordX1, coordY1, coordX2, coordY2);
    QVERIFY((coordX1 < coordX2) && (coordY1 < coordY2));
}

QTEST_MAIN(TestInputProcessor);
