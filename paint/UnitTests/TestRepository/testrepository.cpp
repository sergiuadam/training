#include "testrepository.h"
#include <QtTest>
void TestRepository::testAddShape() {
    Coordinates coords;
    coords.topLeftCoordinateX = 0;
    coords.topLeftCoordinateY = 0;
    coords.bottomRightCoordinateX = 0;
    coords.bottomRightCoordinateY = 0;

    Repository repo;
    const QString color("black");
    repo.addShape(Shape::ShapeTypes::Rectangle,color,coords);

    Shape* sh = repo.m_shapes->at(0);
    QVERIFY(sh->Type() == Shape::ShapeTypes::Rectangle);
    QVERIFY(sh->Color() == color);
    QVERIFY(sh->StartCoordinateX() == coords.topLeftCoordinateX);
    QVERIFY(sh->StartCoordinateY() == coords.topLeftCoordinateY);
    QVERIFY(sh->EndCoordinateX() == coords.bottomRightCoordinateX);
    QVERIFY(sh->EndCoordinateY() == coords.bottomRightCoordinateY);

    QVERIFY_EXCEPTION_THROWN(repo.addShape(Shape::ShapeTypes::Invalid,"nullptr",coords), QException);

}

void TestRepository::testAddText() {
    Coordinates coords;
    coords.topLeftCoordinateX = 0;
    coords.topLeftCoordinateY = 0;
    coords.bottomRightCoordinateX = 0;
    coords.bottomRightCoordinateY = 0;

    Repository repo;
    const QString color("black");
    const QString txt("TEXTTEST");
    repo.addShapeText(color,coords,txt);
    Shape* sh = repo.m_shapes->at(0);
    QVERIFY(sh->StartCoordinateX() == coords.topLeftCoordinateX);
    QVERIFY(sh->StartCoordinateY() == coords.topLeftCoordinateY);
    QVERIFY(sh->EndCoordinateX() == coords.bottomRightCoordinateX);
    QVERIFY(sh->EndCoordinateY() == coords.bottomRightCoordinateY);
    QCOMPARE(sh->Txt().toStdString() , txt.toStdString());
}

QTEST_MAIN(TestRepository);
