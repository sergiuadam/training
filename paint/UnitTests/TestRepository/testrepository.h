#pragma once
#include <QObject>
#include "QtTest"
#include "QObject"
#include "Model/shape.h"
#include "Model/shapemodel.h"
#include "Controller/repository.h"
#include "Controller/shapecreator.h"

class TestRepository : public QObject {
    Q_OBJECT

private slots:
    void testAddShape();
    void testAddText();
};

