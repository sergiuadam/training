#pragma once
#include "QtTest"
#include "QObject"
#include "Controller/filehandler.h"
#include "Controller/repository.h"
#include "Model/shapemodel.h"
#include "LIBXL/include_cpp/libxl.h"

class TestFileHandler : public QObject{
    Q_OBJECT
private slots:
    void testWrite();
    void testRead();
    void testLoadInvalidFile();
};
