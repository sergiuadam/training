#include "testfilehandler.h"
#include <iostream>
using namespace std;
void TestFileHandler::testRead(){
    FileHandler f;
    Repository repo;
    f.loadFromFile(repo, QString("C:/Users/sergiu.adam/Documents/training/tst.xlsx"));
    const ShapeModel* s = repo.getAllShapes();
    for(int i = 0; i < s->size(); i++){
        QVERIFY(s->at(i)->Type() == Shape::ShapeTypes::Rectangle);
        QVERIFY(s->at(i)->Color() == QString("black"));
        QVERIFY(s->at(i)->StartCoordinateX() == -10);
        QVERIFY(s->at(i)->StartCoordinateY() == 35);
        QVERIFY(s->at(i)->EndCoordinateX() == 45);
        QVERIFY(s->at(i)->EndCoordinateY() == 55);
    }
}

void TestFileHandler::testLoadInvalidFile() {
    FileHandler f;
    Repository repo;
    QVERIFY_EXCEPTION_THROWN(f.loadFromFile(repo,"C:/Users/sergiu.adam/Documents/drawingProgram/mainwindow.cpp"), QException);
}
void TestFileHandler::testWrite(){
    FileHandler f;
    using namespace libxl;
    Book* book = xlCreateXMLBook();
    Sheet* sheet = book->addSheet("Sheet1");

    int row = 1;
    for(int i = 0; i < 3; i++){
        sheet->writeNum(row,0,0);
        sheet->writeStr(row, 1, "black");
        sheet->writeNum(row, 2, -10);
        sheet->writeNum(row, 3, 55);
        sheet->writeNum(row, 4, 45);
        sheet->writeNum(row, 5, 35);
        row++;
    }
    book->save("tst.xlsx");
    book->release();
}
QTEST_MAIN(TestFileHandler);
