#include "notifychannel.h"

NotifyChannel::NotifyChannel(SOCKET& notifySocket) {
    this->notifySocket = notifySocket;
    this->start();
}

void NotifyChannel::decodeAndCreate(QByteArray &receivedData) {
    QBuffer buffer(&receivedData);
    buffer.open(QBuffer::ReadOnly);
    QDataStream in(&buffer);
    qint32 type;
    QString color;
    qint32 topLeftCoordinateX;
    qint32 topLeftCoordinateY;
    qint32 bottomRightCoordinateX;
    qint32 bottomRightCoordinateY;
    qint32 messageLength;
    QString text;
    in >> type;
    Shape::ShapeTypes shapeType = static_cast<Shape::ShapeTypes>(type);

    if (shapeType == Shape::ShapeTypes::Text){
        in >> color >> topLeftCoordinateX >> topLeftCoordinateY
           >> bottomRightCoordinateX >> bottomRightCoordinateY
           >> text >> messageLength;

        Coordinates coords;
        coords.topLeftCoordinateX = topLeftCoordinateX;
        coords.topLeftCoordinateY = topLeftCoordinateY;
        coords.bottomRightCoordinateX = bottomRightCoordinateX;
        coords.bottomRightCoordinateY = bottomRightCoordinateY;
        emit shapeReceivedText(color,coords,text);
    }
    else {
        in >> color >> topLeftCoordinateX >> topLeftCoordinateY
           >> bottomRightCoordinateX >> bottomRightCoordinateY
           >> messageLength;

        Coordinates coords;
        coords.topLeftCoordinateX = topLeftCoordinateX;
        coords.topLeftCoordinateY = topLeftCoordinateY;
        coords.bottomRightCoordinateX = bottomRightCoordinateX;
        coords.bottomRightCoordinateY = bottomRightCoordinateY;
        emit shapeReceived(shapeType, color, coords);
    }
}

void NotifyChannel::run() {
    int iResult{};
    do {
        iResult = recv(notifySocket, recvbuf, recvbuflen, 0);
        if (iResult > 0) {
            printf("Bytes received: %d\n", iResult);
            QByteArray receivedData = QByteArray::fromRawData(recvbuf,recvbuflen);
            decodeAndCreate(receivedData);
        }

    } while (iResult > 0);

    // shutdown the connection since we're done
    iResult = shutdown(notifySocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        printf("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(notifySocket);
        exit(0);
    }

    // cleanup
    closesocket(notifySocket);
}
