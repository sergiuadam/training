import xlrd

def openSavedFiles():
    book1 = xlrd.open_workbook("TEST01.xlsx")
    book2 = xlrd.open_workbook("TEST02.xlsx")
    book3 = xlrd.open_workbook("TEST03.xlsx")
    book4 = xlrd.open_workbook("TEST04.xlsx")
    return [book1,book2,book3,book4]

def getSheetOne():
    books = openSavedFiles()
    sheets = []
    for book in books:
        sheets.append(book.sheet_by_index(0))
    return sheets

def test():
    sheets = getSheetOne()
    for currentRowIndex in range (0,sheets[0].nrows):
        for currentColIndex in range(0,5):
            v1 = sheets[0].cell(currentRowIndex, currentColIndex).value
            v2 = sheets[1].cell(currentRowIndex, currentColIndex).value
            v3 = sheets[2].cell(currentRowIndex, currentColIndex).value
            v4 = sheets[3].cell(currentRowIndex, currentColIndex).value
            if v1 != v2 != v3 != v4:
                print("Error: ", currentRowIndex, " , ", currentColIndex)

test()
print("Test passed")
