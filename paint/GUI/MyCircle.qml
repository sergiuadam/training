import QtQuick 2.0

Item {
    id: myCircle
    property QtObject concrete
    Rectangle {
        x: concrete.StartCoordinateX
        y: concrete.StartCoordinateY
        width: concrete.EndCoordinateX - x > concrete.EndCoordinateY - y ? concrete.EndCoordinateX - x : concrete.EndCoordinateY - y
        height: width
        color: concrete.Color
        radius: width
    }
}
