import QtQuick 2.0

Item {
    id: myText
    property QtObject concrete
    Text {
        x: concrete.StartCoordinateX
        y: concrete.StartCoordinateY
        color: concrete.Color
        text: concrete.Txt

    }
}
