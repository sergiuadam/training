//import related modules
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Window 2.2
import cpp_backend 1.0
import cpp_shape 1.0


ApplicationWindow {
    id: root
    width: 1280
    height: 720
    visible: true
    Window{
        id: messageWindow
        flags: Qt.WindowStaysOnTopHint
        visible: false
        modality: Qt.ApplicationModal
        width: 200
        Rectangle {
            color: "lightskyblue"
            anchors.fill: parent
            Text {
                id: messageWindowText
                text: "Hello !"
                color: "red"
                anchors.centerIn: parent
            }
        }
        Button{
            text: "OK, SORRY";
            width: parent.width
            onClicked: messageWindow.close();
        }
    }

    Backend{
        id: canal
        onInvalidShapeError: {messageWindowText.text = "Invalid shape given ! Please type correct input"; messageWindow.visibile = true}
        onCouldNotSaveError: {messageWindowText.text = "Could not save file !"; messageWindow.visible = true;}
        onCouldNotLoadFileError: {messageWindowText.text = "Invalid file loaded"; messageWindow.visible = true;}
        onDrawNewShape: {}
        onAllShapesChanged: {}

        function getSelectedColor(){
            var color;
            if (buttonBlack.checked === true)
                color = "black";
            if (buttonRed.checked === true)
                color = "red";
            if (buttonBlue.checked === true)
                color = "blue";
            if (buttonPink.checked === true)
                color = "pink";
            if (buttonGreen.checked === true)
                color = "green";
            return color;
        }

        function getSelectedType(){
            var type;
            if (buttonRectangle.checked === true)
                type = Cppshape.Rectangle;
            if (buttonSquare.checked === true)
                type = Cppshape.Square;
            if (buttonCircle.checked === true)
                type = Cppshape.Circle;
            if (buttonLine.checked === true){
                type = Cppshape.Line;
            }
            if (buttonPoint.checked === true)
                type = Cppshape.Point;
            if (buttonText.checked === true) {
                type = Cppshape.Text
                inputText.x = mouseArea.mouseX
                inputText.y = mouseArea.mouseY
                inputText.visible = true
                inputText.forceActiveFocus()
            }
        }

        function requestShape() {
            var type = getSelectedType();
            var color = getSelectedColor();
            shapeCreator(type, color);
        }
        function askRectangle(color){
            canal.addButtonClicked(Cppshape.Rectangle, color);
        }
        function askCircle(color){
            canal.addButtonClicked(Cppshape.Circle, color)
        }
        function askText(color){
            canal.addButtonClicked(color, inputText.text);
        }
        function askSquare(color){
            canal.addButtonClicked(Cppshape.Square, color);
        }
        function askLine(color){
            canal.addButtonClicked(Cppshape.Line, color);
        }
        function askPoint(color){
            canal.addButtonClicked(Cppshape.Point, color)
        }
        function askDefault(){
            // ??????
        }
        function shapeCreator(type, color){
            switch (type){
            case Cppshape.Rectangle : askRectangle(color); break;
            case Cppshape.Circle : askCircle(color); break;
            case Cppshape.Text :  break;
            case Cppshape.Square : askSquare(color); break;
            case Cppshape.Line : askLine(color); break;
            case Cppshape.Point : askPoint(color); break;
            default: askDefault()
            }
        }
    }
    FileDialog{
        id: fileDialogSave
        title: "Save your drawing board !"
        onAccepted: canal.saveButtonClicked(fileDialogSave.fileUrl)
        selectExisting: false
        selectMultiple: false
        selectFolder: false

    }
    FileDialog{
        id: fileDialogLoad
        title: "Load your drawing board !"
        onAccepted: canal.loadButtonClicked(fileDialogLoad.fileUrl)
        selectExisting: true
        selectMultiple: false
        selectFolder: false
    }
    Row{
        Column {
            Row {
                Column {
                    ButtonGroup {
                        id: shapeButtons
                    }
                    RadioButton {
                        id: buttonRectangle
                        text: qsTr("Rectangle")
                        ButtonGroup.group: shapeButtons
                    }
                    RadioButton {
                        id: buttonSquare
                        text: qsTr("Square")
                        ButtonGroup.group: shapeButtons
                    }
                    RadioButton {
                        id: buttonCircle
                        text: qsTr("Circle")
                        ButtonGroup.group: shapeButtons
                    }
                    RadioButton {
                        id: buttonPoint
                        text: qsTr("Point")
                        ButtonGroup.group: shapeButtons
                    }
                    RadioButton {
                        id: buttonLine
                        text: qsTr("Line")
                        ButtonGroup.group: shapeButtons
                    }
                    RadioButton {
                        id: buttonText
                        text: qsTr("Text")
                        ButtonGroup.group: shapeButtons
                    }
                }
                Column {
                    ButtonGroup {
                        id: colorButtons
                    }
                    RadioButton {
                        id: buttonBlack
                        text: qsTr("Black")
                        ButtonGroup.group: colorButtons
                    }
                    RadioButton {
                        id: buttonRed
                        text: qsTr("Red")
                        ButtonGroup.group: colorButtons
                    }
                    RadioButton {
                        id: buttonBlue
                        text: qsTr("Blue")
                        ButtonGroup.group: colorButtons
                    }
                    RadioButton {
                        id: buttonPink
                        text: qsTr("Pink")
                        ButtonGroup.group: colorButtons
                    }
                    RadioButton {
                        id: buttonGreen
                        text: qsTr("Green")
                        ButtonGroup.group: colorButtons
                    }
                }

            }
            Button{
                id: buttonSave
                width: 200
                height: 100
                onClicked: {fileDialogSave.visible = true}
                Rectangle{
                    gradient: Gradient {
                        GradientStop {
                            position: 0.0
                            SequentialAnimation on color {
                                loops: Animation.Infinite
                                ColorAnimation { from: "purple"; to: "cyan"; duration: 500 }
                            }
                        }
                        GradientStop {
                            position: 1.0
                            SequentialAnimation on color {
                                loops: Animation.Infinite
                                ColorAnimation { from: "cyan"; to: "orange"; duration: 500 }
                            }
                        }
                    }
                }
                Text{
                    id: textButtonSave
                    text: "Save drawing board"
                    anchors.centerIn: buttonSave
                    font.family: "Times New Roman"
                    font.pointSize: 14
                    SequentialAnimation on color{
                        loops: Animation.Infinite
                        ColorAnimation {
                            from: "pink"
                            to: "black"
                            duration: 500
                        }
                        ColorAnimation {
                            from: "yellow"
                            to: "green"
                            duration: 500
                        }
                        ColorAnimation {
                            from: "black"
                            to: "pink"
                            duration: 500
                        }
                    }
                }
            }
            Button{
                id: buttonLoad
                width: 200
                height: 100
                onClicked: {fileDialogLoad.visible=true}
                Rectangle{
                    gradient: Gradient {
                        GradientStop {
                            position: 0.0
                            SequentialAnimation on color {
                                loops: Animation.Infinite
                                ColorAnimation { from: "red"; to: "cyan"; duration: 500 }
                            }
                        }
                        GradientStop {
                            position: 1.0
                            SequentialAnimation on color {
                                loops: Animation.Infinite
                                ColorAnimation { from: "cyan"; to: "red"; duration: 500 }
                            }
                        }
                    }
                }
                Text{
                    id: textButtonLoad
                    text: "Load drawing board"
                    anchors.centerIn: buttonLoad
                    font.family: "Times New Roman"
                    font.pointSize: 14
                    SequentialAnimation on color{
                        loops: Animation.Infinite
                        ColorAnimation{
                            from: "pink"
                            to: "yellow"
                            duration: 500
                        }
                        ColorAnimation {
                            from: "black"
                            to: "green"
                            duration: 500
                        }
                        ColorAnimation {
                            from: "green"
                            to: "black"
                            duration: 500
                        }
                    }
                }
            }
        }
        Rectangle{
            id: drawingBoard
            width: root.width-200.1
            height: root.height-0.1
            border.width: 5
            border.color: "black"
            Text {
                id: coordHelp1
                text: qsTr("x: 0 y: 0")
                anchors.topMargin: drawingBoard.Top
            }
            MouseArea{
                id: mouseArea
                height: parent.height
                width: parent.width
                onPressed: canal.saveInitialMouseCoordinates(mouseArea.mouseX, mouseArea.mouseY)
                onReleased: { canal.saveFinalMouseCoordinates(mouseArea.mouseX, mouseArea.mouseY); canal.requestShape(); }
            }
            Connections {
                target: canal
                onAllShapesChanged: repetetorul.model = canal.allShapes
            }
            Repeater{
                id: repetetorul
                model: canal.allShapes
                delegate: Loader {
                    id: myLoader
                    function getMyComponent(type){
                        switch (type){
                        case Cppshape.Rectangle: return componentRectangle;
                        case Cppshape.Circle: return componentCircle;
                        case Cppshape.Square: return componentSquare;
                        case Cppshape.Point: return componentPoint;
                        case Cppshape.Text: return componentText;
                        case Cppshape.Line: return componentLine;
                        }
                    }
                    sourceComponent: getMyComponent(soConcrete.Type)
                    readonly property QtObject soConcrete: qtObject
                    Component {
                        id: componentRectangle
                        MyRectangle{
                            concrete: soConcrete
                        }
                    }
                    Component {
                        id: componentCircle
                        MyCircle{
                            concrete: soConcrete
                        }
                    }
                    Component {
                        id: componentSquare
                        MySquare{
                            concrete: soConcrete
                        }
                    }
                    Component {
                        id: componentPoint
                        MyPoint{
                            concrete: soConcrete
                        }
                    }
                    Component {
                        id: componentText
                        MyText{
                            concrete: soConcrete
                        }
                    }
                    Component{
                        id: componentLine
                        MyLine{
                            concrete: soConcrete
                        }
                    }
                }
            }
            TextInput {
                id: inputText
                text: qsTr("")
                onEditingFinished: {visible = false; canal.askText(canal.getSelectedColor()); text = ""}
            }
        }
    }
}
