import QtQuick 2.0

Item {
    id: myLine
    property QtObject concrete
    function latura(a,b){
        return Math.sqrt(a*a+b*b)
    }
    function angleBetweenPoints(a,b) {
        return Math.atan2(a, b) * 180 / Math.PI;
    }
    Rectangle{
        x: concrete.StartCoordinateX
        y: concrete.StartCoordinateY
        width: latura(concrete.EndCoordinateY-concrete.StartCoordinateY, concrete.EndCoordinateX-concrete.StartCoordinateX)
        height: 2
        color: concrete.Color
        rotation: angleBetweenPoints(Math.abs(concrete.EndCoordinateY-concrete.StartCoordinateY), Math.abs(concrete.EndCoordinateX-concrete.StartCoordinateX))
    }
}

