import QtQuick 2.0
import cpp_backend 1.0
import cpp_shape 1.0

Item {
    id: myRectangle
    property QtObject concrete
    Rectangle{
        x: concrete.StartCoordinateX
        y: concrete.StartCoordinateY
        width: concrete.EndCoordinateX-concrete.StartCoordinateX
        height: concrete.EndCoordinateY-concrete.StartCoordinateY
        color: concrete.Color
    }
}
