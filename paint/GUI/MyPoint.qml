import QtQuick 2.0

Item {
    id: myPoint
    property QtObject concrete
    Rectangle {
        x: concrete.StartCoordinateX
        y: concrete.StartCoordinateY
        width: 5
        height: 5
        color: concrete.Color
        radius: 5
    }
}
