#pragma once

#include <QQmlApplicationEngine>
#include <QObject>
#include <QString>
#include <QQmlListProperty>
#include "../Controller/controller.h"
#include "../Model/shapemodel.h"
#include "validator.h"
#include <iostream>
/*
 * BackEnd is the communication layer between the QML interface and C++ business logic.
 * Through BackEnd signals,as well as messages, are sent and received.
*/
class BackEnd : public QObject {
    Q_OBJECT
    Q_PROPERTY(ShapeModel* allShapes READ allShapes NOTIFY noNotify)
private:
    Controller* m_appController;
    Coordinates m_coordinates;
    Validator m_validator;

    void populateShapes();

public slots:
    void addButtonClicked(const int type, const QString& color);
    void addButtonClicked(const QString& color, const QString& text);
    void saveButtonClicked(const QUrl&);
    void loadButtonClicked(const QUrl&);

    void saveInitialMouseCoordinates(const int mouseX, const int mouseY);
    void saveFinalMouseCoordinates(const int mouseX, const int mouseY);

signals:
    void invalidShapeError();
    void couldNotSaveError();
    void couldNotLoadFileError();
    void drawNewShape();
    void allShapesChanged();

    void noNotify();

public:
    explicit BackEnd(QObject *parent = nullptr);
    ~BackEnd();
    ShapeModel* allShapes();

};
