#include "backend.h"
#include <utility>
#include <QException>
#include "iostream"
using namespace std;

BackEnd::BackEnd(QObject *parent) : QObject(parent) {
    m_appController = new Controller();
}

BackEnd::~BackEnd() {
    delete m_appController;
}

ShapeModel* BackEnd::allShapes() {
    return m_appController->getAllShapes();
}

void BackEnd::addButtonClicked(const int type, const QString& color) {
    try{
        if(m_validator.isInputValid(m_coordinates) == false){
            throw QException();
        }
        m_appController->addNewShape(Shape::ShapeTypes(type), color, m_coordinates);
        emit drawNewShape();
    }
    catch(...){
        emit invalidShapeError();
    }
}

void BackEnd::addButtonClicked(const QString& color, const QString& text) {
    try{
        if(m_validator.isInputValid(m_coordinates) == false){
            throw QException();
        }
        m_appController->addNewShape(color, m_coordinates, text);
        emit drawNewShape();
    }
    catch(...){
        emit invalidShapeError();
    }
}

void BackEnd::saveButtonClicked(const QUrl& message) {
    try {
        QString filePath = message.path();
        filePath.remove(0,1);
        m_appController->saveToFile(filePath);
    } catch (...) {
        emit couldNotSaveError();
    }
}

void BackEnd::loadButtonClicked(const QUrl& message) {
    try {
        QString filePath = message.path();
        filePath.remove(0,1);
        m_appController->loadFromFile(filePath);
        emit drawNewShape();
    } catch (...) {
        emit couldNotLoadFileError();
    }
}

void BackEnd::saveInitialMouseCoordinates(const int mouseX, const int mouseY) {
    m_coordinates.topLeftCoordinateX = mouseX;
    m_coordinates.topLeftCoordinateY = mouseY;
}

void BackEnd::saveFinalMouseCoordinates(const int mouseX, const int mouseY) {
    m_coordinates.bottomRightCoordinateX = mouseX;
    m_coordinates.bottomRightCoordinateY = mouseY;
}
