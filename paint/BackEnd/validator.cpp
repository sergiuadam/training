#include "validator.h"

Validator::Validator() = default;

bool Validator::isInputValid(Coordinates coords){
    try {
        const int tx = coords.topLeftCoordinateX;
        const int ty = coords.topLeftCoordinateY;
        if(tx <= 0 or ty <= 0){
            throw;
        }
        const int bx = coords.bottomRightCoordinateX;
        const int by = coords.bottomRightCoordinateY;
        if (bx <= 0 or by <= 0){
            throw;
        }
        return true;
    } catch (...) {
        return false;
    }
}
