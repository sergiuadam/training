#include <iostream>
#include <smartpointer.h>
using namespace std;

typedef SmartPointer<int> SmartPtr;

SmartPtr testFunction( SmartPtr* ptr1, SmartPtr& ptr2, SmartPtr ptr3) {
        cout<<"\n\n TEST FUNCTION: "<<endl;
        SmartPtr internalPtr;
        *ptr1 = ptr2;
        ptr2 = ptr3;
        internalPtr = *ptr1;
        cout<<"END OF TEST FUNCTION \n\n";
        return internalPtr;
}

int main() {
    SmartPtr ptr1;
    SmartPtr ptr2;
    //SmartPtr ptr3;
    SmartPtr ptr4 = testFunction( &ptr1, ptr2,SmartPtr());
    return 0;
}

/*
void function2(SmartPointer<int>& var){
    cout<<*var<<endl;
}
void function(SmartPointer<int> var){
    cout<<"fnc: "<<*var<<endl;
    function2(var);
}
int main() {
    SmartPointer<int> smartptr;
    *smartptr = 20;
    SmartPointer<int> x = smartptr;
    SmartPointer<int>otherPtr;
    otherPtr = smartptr;
    function(otherPtr);
    cout<<*x<<endl;
}*/
