#ifndef SMARTPOINTER_H
#define SMARTPOINTER_H
#include <iostream>
using namespace std;

template<typename T>
class SmartPointer {
private:
    T* ptr;
    int* refCount;
public:
    //Constructor
    SmartPointer<T>(){
        ptr = new T;
        if (ptr == nullptr){
            cout<<"Failed to allocate memory..."<<endl;
            return;
        }
        refCount = new int;
        *refCount = 1;
        cout<<"Constructor on "<<this<<"    points to "<<ptr<<endl;
    }
    //Destructor
    ~SmartPointer<T>(){
        cout<<"\n Destructor called on "<<this<<endl;
        cout<<"Reference at start of destructor: "<<*refCount<<endl;
        *refCount-=1;
        if(*refCount == 0){
            cout<<"MEMORY ZONE UNREFERENCED IN DESTRUCTOR, LAST POINTER: "<<this<<" TO ZONE "<<ptr<<endl;
            delete ptr;
            delete refCount;
        }
    }
    //Copy constructor
    SmartPointer<T>(const SmartPointer<T>& otherPtr){
        cout<<"COPY CONSTRUCTOR CALLED ON "<<this<<" , COPYING FROM "<<&otherPtr<<endl;
        ptr = otherPtr.ptr;
        refCount = otherPtr.refCount;
        *refCount+=1;

    }
    //Copy assignment
    SmartPointer<T>& operator=(SmartPointer<T>& otherPtr){
        cout<<"COPY ASSIGNMENT CALLED ON "<<this<<" , COPYING FROM "<<&otherPtr<<endl;
        cout<<"Reference at start of copy assignment: "<<*refCount<<endl;
        *refCount-=1;
        if (*refCount == 0){
            cout<<"MEMORY ZONE UNREFERENCED IN DESTRUCTOR, LAST POINTER: "<<this<<" TO ZONE "<<ptr<<endl;
            delete ptr;
            delete refCount;
        }
        this->refCount = otherPtr.refCount;
        this->ptr = otherPtr.ptr;
        *refCount+=1;
        return *this;
    }

    SmartPointer<T>(SmartPointer<T>&& x){
        cout<<"MOVE CONSTRUCTOR: "<<endl;
        ptr = x.ptr;
        refCount = x.refCount;
    }
    SmartPointer<T> operator=(SmartPointer<T>&& x){
        cout<<"MOVE ASSIGNMENT "<<endl;
        ptr = x.ptr;
        refCount = x.refCount;
    }
    T& operator*(){
        return *ptr;
    }
};
#endif // SMARTPOINTER_H
